const { ethers, upgrades } = require("hardhat");

async function main() {
  let skies, adaran, gateway;
  
  console.log('Deploy contracts:')
  const [owner, otherAccount] = await ethers.getSigners();
  const SKIES = await ethers.getContractFactory("Skies");
  skies = await SKIES.deploy([owner.address], ["1000000000000000000000000"]);
  console.log(`Skies at ${skies.address}`)

  const ADARAN = await ethers.getContractFactory("Adaran");
  adaran = await ADARAN.deploy();
  console.log(`Adaran at ${adaran.address}`)

  const Gateway = await ethers.getContractFactory("PoolGateway");
  gateway = await upgrades.deployProxy(Gateway, [skies.address, adaran.address]);
  console.log(`Gateway at ${gateway.address}`)

  const HERO = await ethers.getContractFactory("Hero");
  hero = await HERO.deploy(skies.address,gateway.address);
  console.log(`Hero at ${hero.address}`)

  const ARMOR = await ethers.getContractFactory("Armor");
  armor = await ARMOR.deploy(skies.address,gateway.address);
  console.log(`Armor at ${armor.address}`)

  const WEAPON = await ethers.getContractFactory("Weapon");
  weapon = await WEAPON.deploy(skies.address,gateway.address);
  console.log(`Weapon at ${weapon.address}`)

  const POTION = await ethers.getContractFactory("Potion");
  potion = await POTION.deploy(skies.address,gateway.address);
  console.log(`Potion at ${potion.address}`)

}

main();