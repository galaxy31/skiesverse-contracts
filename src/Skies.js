const { expect } = require("chai");

describe("Skies", function () {
let skies;
    it("Deploy", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const SKIES = await ethers.getContractFactory("Skies");
      skies = await SKIES.deploy([owner.address], ["1000000000000000000000000000"]);
    });

    it('has a name', async function () {
      expect(await skies.name()).to.equal('Skies');
    });

    it('has a symbol', async function () {
      expect(await skies.symbol()).to.equal('SKI');
    });

    it('has 18 decimals', async function () {
      expect(await skies.decimals()).to.equal(18);
    });

    it("Get balance of owner", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      expect( await skies.balanceOf(owner.address)).to.equal("1000000000000000000000000000") ;
    });

    it("Get totalSupply", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      expect( await skies.totalSupply()).to.equal("1000000000000000000000000000") ;
    });

    it("Transfer from owner to other account", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await skies.transfer(otherAccount.address,"500000000000000000000000000");
      expect( await skies.balanceOf(otherAccount.address)).to.equal("500000000000000000000000000");
    });

    it("Approve from owner to other account", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await skies.approve(otherAccount.address,"500000000000000000000000000");
      expect( await skies.allowance(owner.address,otherAccount.address)).to.equal("500000000000000000000000000");
    });

    it("TransferFrom from owner to other account", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await skies.connect(otherAccount).transferFrom(owner.address,otherAccount.address,"500000000000000000000000000");
      expect( await skies.balanceOf(otherAccount.address)).to.equal("1000000000000000000000000000");
    });

    it('reverts transfer from zero balance', async function () {
      const [owner, otherAccount] = await ethers.getSigners();
        await expect(
          skies.transfer(otherAccount.address,"500000000000000000000000000" ),
        ).to.be.revertedWith('ERC20: transfer amount exceeds balance');
      });

    it('reverts decreaseAllowance from zero balance', async function () {
      const [owner, otherAccount] = await ethers.getSigners();
        await expect(
          skies.decreaseAllowance(otherAccount.address,"500000000000000000000000000" ),
        ).to.be.revertedWith('ERC20: decreased allowance below zero');
      });

});
