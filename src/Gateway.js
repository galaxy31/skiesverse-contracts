const  hre  = require('hardhat');
const { expect } = require("chai");
const { time } = require("@nomicfoundation/hardhat-network-helpers");


const CHAIN_ID=31337

describe("Gateway", function () {
  let adaran;
  let skies;
  let gateway;

    it("Deploy skies", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const SKIES = await ethers.getContractFactory("Skies");
      skies = await SKIES.deploy([owner.address], ["1000000000000000000000000000"]);
    });

    it("Deploy adaran", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const ADARAN = await ethers.getContractFactory("Adaran");
      adaran = await ADARAN.deploy();
    });

    it("Deploy Gateway", async function () {
      const Gateway = await ethers.getContractFactory("PoolGateway");
      gateway = await upgrades.deployProxy(Gateway, [skies.address, adaran.address]);
    });

    it("Upgrade Gateway", async function () {
      const Gateway2 = await ethers.getContractFactory("PoolGateway");
      gateway = await upgrades.upgradeProxy(gateway.address, Gateway2);
    });



    it("Mint with permit to owner", async function () {
      const [owner, addr1] = await ethers.getSigners();
      //PermitMint(address owner,uint256 value,uint256 nonce)
      const value="1000000000000000000000000000"
      const nonce=(await adaran.nonces(owner.address)).toString()
//===================  BACKEND starts
    const msgParams = JSON.stringify({
    domain: {
      chainId: CHAIN_ID,
      name: 'Adaran',
      verifyingContract: adaran.address,
      version: '1',
    },

    message: {
      owner: owner.address,
      value: value,
      nonce: nonce
    },
    primaryType: 'PermitMint',
    types: {
      EIP712Domain: [
        { name: 'name', type: 'string' },
        { name: 'version', type: 'string' },
        { name: 'chainId', type: 'uint256' },
        { name: 'verifyingContract', type: 'address' },
      ],
      PermitMint: [
        { name: 'owner', type: 'address' },
        { name: 'value', type: 'uint256' },
        { name: 'nonce', type: 'uint256' }
      ]
    },
  })

    const signature=await hre.network.provider.request({
      method: "eth_signTypedData_v4",
      params: [owner.address,msgParams],
    });
    const r = signature.substr(0,66)
    const s = "0x" + signature.substr(66,64)
    const v = Number("0x" + signature.substr(-2))
//===================  BACKEND ends
//==================== FRONTEND starts
    await adaran.permitMint(owner.address, value, v, r, s)
//==================== FRONTEND ends
      expect( await adaran.balanceOf(owner.address)).to.equal(value) ;
    });

    it("Give signer role to account2", async function () {
      const [owner] = await ethers.getSigners();
      const dev_role= await gateway.DEVELOPER() 
      const signer_role= await gateway.SIGNER() 
      await gateway.grantRole( signer_role, owner.address)
      expect( await gateway.hasRole( signer_role, owner.address)).to.equal(true) ;
 
    });

    it("Transfer skies from owner to addr1", async function () {
      const [owner,addr1] = await ethers.getSigners();
      const value="10000000000000000000000000"
      await skies.transfer( addr1.address, value)
      expect( await skies.balanceOf( addr1.address)).to.equal(value) ;
 
    });


    it("Spend skies tokens from addr1", async function () {
      const [owner, addr1] = await ethers.getSigners();
      const value="10000000000000000000000000"
      const nonce=(await adaran.nonces(addr1.address)).toString()
      const msgParams = JSON.stringify({
    domain: {
      chainId: CHAIN_ID,
      name: 'PoolGateway',
      verifyingContract: gateway.address,
      version: '1',
    },

    message: {
      from: addr1.address,
      token: skies.address,
      value: value,
      eventId: "1",
      nonce: nonce
    },
    primaryType: 'spendTokens',
    types: {
      EIP712Domain: [
        { name: 'name', type: 'string' },
        { name: 'version', type: 'string' },
        { name: 'chainId', type: 'uint256' },
        { name: 'verifyingContract', type: 'address' },
      ],
      spendTokens: [
        { name: 'from', type: 'address' },
        { name: 'token', type: 'address' },
        { name: 'value', type: 'uint256' },
        { name: 'eventId', type: 'uint256' },
        { name: 'nonce', type: 'uint256' }
      ]
    },
  })

    const signature=await hre.network.provider.request({
      method: "eth_signTypedData_v4",
      params: [owner.address,msgParams],
    });
    const r = signature.substr(0,66)
    const s = "0x" + signature.substr(66,64)
    const v = Number("0x" + signature.substr(-2))
    await skies.connect(addr1).approve(gateway.address, value)
    await gateway.connect(addr1).spendTokens(addr1.address, skies.address, value, "1" , v, r, s)
      
      expect( await skies.balanceOf(gateway.address)).to.equal(value);
    });




});
