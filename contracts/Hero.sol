//SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/cryptography/EIP712.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "./Nonces.sol";

error ErrorSendingEthersToPool();
error InvalidTokenAddress();
error InvalidPoolAddress();
error InvalidSignature();

contract Hero is ERC721, AccessControl,  EIP712("Hero","1"), Nonces{
    address private immutable _token;
    address private immutable _pool;
    bytes32 public constant SIGNER = keccak256("SIGNER");
    string private _baseTokenURI;
    // solhint-disable-next-line var-name-mixedcase
    bytes32 private constant _PERMIT_TYPEHASH1 =
    keccak256("PermitMintEther(address owner,uint256 tokenId,uint256 value,uint256 nonce)");

    bytes32 private constant _PERMIT_TYPEHASH2 =
    keccak256("PermitMint(address owner,uint256 tokenId,uint256 value,uint256 nonce)");

    /**
     * @dev initialize contract with base tokens `token` as Skies and token receiver as  `pool`.
     *
     */
    constructor(address token, address pool) ERC721("Hero","HRO"){
        if (token==address(0)) revert InvalidTokenAddress();
        if (pool==address(0)) revert InvalidPoolAddress();
        _token=token;
        _pool=pool;
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(SIGNER, msg.sender);

    }

    /**
     * @dev Return token's URI=base+ `tokenId` + '.json'.
     *
     */

    function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
        return  string(abi.encodePacked(_baseTokenURI,Strings.toString(tokenId), ".json" ));
    }

    /**
     * @dev Return `_baseTokenURI` as the base for token's URI.
     *
     */

    function _baseURI() internal view virtual override returns (string memory) {
        return  _baseTokenURI;
    }
    /**
     * @dev Sets `_baseTokenURI` as the base for token's URI.
     *
     */
    function setBaseURI(string memory uri) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _baseTokenURI=uri;
    }

    /**
     * @dev Mints for `to` `tokenId` token for `value` ethers,
     * given ``signer``'s signed approval.
     *
     *
     * Emits an {Transfer} event.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     * - `v`, `r` and `s` must be a valid `secp256k1` signature from `owner`
     * over the EIP712-formatted function arguments.
     * - the signature must use ``owner``'s current nonce (see {nonces}).
     *
     * For more information on the signature format, see the
     * https://eips.ethereum.org/EIPS/eip-2612#specification[relevant EIP
     * section].
     */
    function permitMintForEthers(
        address to,
        uint256 tokenId,
        uint256 value,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external payable virtual {
        require(msg.value>=value);
        bytes32 structHash = keccak256(abi.encode(_PERMIT_TYPEHASH1, to, tokenId, value, _useNonce(to)));
        bytes32 hash = _hashTypedDataV4(structHash);
        address signer = ECDSA.recover(hash, v, r, s);
        if(!hasRole(SIGNER, signer)) revert InvalidSignature();
         (bool done,)=payable(_pool).call{value: value}('');
        if (!done) revert ErrorSendingEthersToPool();
        _mint(to, tokenId);
    }
    /**
     * @dev Mints for `to` `tokenId` token for `value` tokens,
     * given ``signer``'s signed approval.
     *
     *
     * Emits an {Transfer} event.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     * - `v`, `r` and `s` must be a valid `secp256k1` signature from `owner`
     * over the EIP712-formatted function arguments.
     * - the signature must use ``owner``'s current nonce (see {nonces}).
     *
     * For more information on the signature format, see the
     * https://eips.ethereum.org/EIPS/eip-2612#specification[relevant EIP
     * section].
     */
    function permitMint(
        address to,
        uint256 tokenId,
        uint256 value,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external virtual {
        bytes32 structHash = keccak256(abi.encode(_PERMIT_TYPEHASH2, to, tokenId, value, _useNonce(to)));
        bytes32 hash = _hashTypedDataV4(structHash);
        address signer = ECDSA.recover(hash, v, r, s);
        if(!hasRole(SIGNER, signer)) revert InvalidSignature();
        SafeERC20.safeTransferFrom(IERC20(_token), _msgSender(),_pool,value);
        _mint(to, tokenId);
    }
    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override(AccessControl,ERC721)  returns (bool) {
        return super.supportsInterface(interfaceId);
    }

}