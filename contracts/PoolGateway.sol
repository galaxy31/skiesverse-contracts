//SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/EIP712Upgradeable.sol";


error IllegalAmount();
contract PoolGateway is Initializable, AccessControlUpgradeable,  EIP712Upgradeable{

    struct Stake{
        address from;
        uint256 amount;
        uint256 finish_amount;
        uint40 start;
        uint40 end;
        uint8 reward;
    }

    address private _skies_token;
    address private _adaran_token;
    uint256 private _totalSkiesBalance;
    uint256 private _withdrawed;
    uint256 private _rewarded;
    uint256 private _staked;
    uint256 constant DEVELOPERS_POOL=30; //30%
    uint256 constant REWARD_POOL=20; //20%
    uint256 constant STAKING_POOL=50; //50%
    bytes32 public constant DEVELOPER = keccak256("DEVELOPER");
    bytes32 public constant SIGNER = keccak256("SIGNER");
    event DevelopersWithdraw(address indexed, address indexed token, uint256 amount);
    event DevelopersWithdrawEthers(address indexed, uint256 amount);
    event TokensSpended(address indexed from,address indexed token, uint256 indexed value, uint256 eventId);
    event TokensWithdrawed(address indexed to,address indexed token, uint256 indexed value, uint256 eventId);
    event StakeAdded(address indexed to,uint40 start, uint40 end, uint256 indexed reward, uint256 amount);
    event StakeRemoved(address indexed from,uint256 amount);

    using CountersUpgradeable for CountersUpgradeable.Counter;
    mapping(address => CountersUpgradeable.Counter) private _nonces;

    mapping(address => Stake[]) public stakes;
    // solhint-disable-next-line var-name-mixedcase
    bytes32 private constant _PERMIT_TYPEHASH =
    keccak256("spendTokens(address from,address token,uint256 value,uint256 eventId,uint256 nonce)");

    bytes32 private constant _PERMIT_TYPEHASH_2 =
    keccak256("getTokens(address to,address token,uint256 value,uint256 eventId,uint256 nonce)");

    bytes32 private constant _PERMIT_TYPEHASH_3 =
    keccak256("addStake(address to,uint40 start,uint40 end,uint8 reward,uint256 amount,uint256 nonce)");

    /**
     * @dev initialize upgradeable contract with two base tokens `token1` as Skies (staking is possible), `token2` as Adaran.
     *
     */
    function initialize(address token1, address token2) initializer public{
        require(token1!=address(0),"Invalid token1 address");
        require(token2!=address(0),"Invalid token2 address");
        _skies_token=token1;
        _adaran_token=token2;
        __EIP712_init_unchained("PoolGateway","1");
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(SIGNER, msg.sender);
    }


    /**
     * @dev Send tokens `token` from `from`  for `value` tokens to PoolGateway,
     * given ``signer``'s signed approval.
     *
     *
     * Emits an {TokensSpended} event with relevant `EventId` to check in backend.
     *
     */
    function spendTokens(
        address from,
        address token,
        uint256 value,
        uint256 eventId,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external virtual {
        bytes32 structHash = keccak256(abi.encode(_PERMIT_TYPEHASH, from, token, value, eventId, _useNonce(from)));
        bytes32 hash = _hashTypedDataV4(structHash);
        address signer = ECDSAUpgradeable.recover(hash, v, r, s);
        require(hasRole(SIGNER, signer), "mintPermit: invalid signature");
        if (token==_skies_token) _totalSkiesBalance+=value;
        SafeERC20Upgradeable.safeTransferFrom(IERC20Upgradeable(token), _msgSender(),address(this),value);
        emit TokensSpended(from,token, value,eventId);
    }

    /**
     * @dev Get tokens `token` from PoolGateway to `to`  for `value` tokens,
     * given ``signer``'s signed approval.
     *
     * Emits an {TokensWithdrawed} event with relevant `EventId` to check in backend.
     *
     */
    function getTokens(
        address to,
        address token,
        uint256 value,
        uint256 eventId,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external virtual {
        bytes32 structHash = keccak256(abi.encode(_PERMIT_TYPEHASH_2, to, token, value, eventId, _useNonce(to)));
        bytes32 hash = _hashTypedDataV4(structHash);
        address signer = ECDSAUpgradeable.recover(hash, v, r, s);
        require(hasRole(SIGNER, signer), "mintPermit: invalid signature");
        SafeERC20Upgradeable.safeTransferFrom(IERC20Upgradeable(token), address(this),to,value);
        emit TokensWithdrawed(to,token, value,eventId);
    }
    receive() payable external{
    }
    /**
     * @dev Add stake from `user` started at `start` and ended at `end` with finish amount =  `amount` + `reward`/100
     * given ``signer``'s signed approval.
     *
     * Emits `StakeAdded` event
     */
    function addStake(
        address user,
        uint40 start,
        uint40 end,
        uint8 reward,
        uint256 amount,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external virtual {
        if (amount == 0) revert IllegalAmount();
        bytes32 structHash = keccak256(abi.encode(_PERMIT_TYPEHASH_3, user, start,end,reward, amount,_useNonce(user)));
        bytes32 hash = _hashTypedDataV4(structHash);
        address signer = ECDSAUpgradeable.recover(hash, v, r, s);
        require(hasRole(SIGNER, signer), "mintPermit: invalid signature");
        SafeERC20Upgradeable.safeTransferFrom(IERC20Upgradeable(_skies_token), _msgSender(), address(this),amount);
        _totalSkiesBalance+=amount;
        Stake memory stake;
        stake.from=user;
        stake.start=start;
        stake.end=end;
        stake.amount=amount;
        stake.reward=reward;
        stake.finish_amount = amount * (100 + reward) / 100;
        stakes[user].push(stake);
        emit StakeAdded(user, start, end, reward, amount);
    }

    /**
     * @dev Remove stake `id` and receive skies + interest
     *
     * Emits `StakeRemoved` event
     */
    function finishStake(uint256 id)external{
        require(stakes[msg.sender][id].amount > 0, "Illegal stake ID");
        require(stakes[msg.sender][id].finish_amount + _staked <= _totalSkiesBalance * STAKING_POOL / 100,"Too much amount" );
        require(stakes[msg.sender][id].end > block.timestamp, "Stake is not ended");
        SafeERC20Upgradeable.safeTransfer(IERC20Upgradeable(_skies_token), _msgSender(),stakes[msg.sender][id].finish_amount);
        emit StakeRemoved(msg.sender,stakes[msg.sender][id].finish_amount);
    }
    
    /**
     * @dev Withdraw Skies tokens for `amount` for Role DEVELOPER up to 30%
     *
     * Emits `DevelopersWithdrawEthers`  event
     */

    function withdrawSkies(uint256 amount)external onlyRole(DEVELOPER){
        require(amount + _withdrawed <= _totalSkiesBalance * DEVELOPERS_POOL / 100,"Too much amount" );
        _withdrawed += amount;
        SafeERC20Upgradeable.safeTransfer(IERC20Upgradeable(_skies_token), _msgSender(),amount);
        emit DevelopersWithdraw(msg.sender, _skies_token, amount);
    }


     /**
     * @dev Withdraw ethers for `amount` for Role DEVELOPER
     *
     * Emits `DevelopersWithdrawEthers`  event
     */

    function withdrawEthers(uint256 amount)external onlyRole(DEVELOPER){
        payable(msg.sender).transfer(amount);
        //if(!done) revert("Error send ethers");
        emit DevelopersWithdrawEthers(msg.sender, amount);
    }

     /**
     * @dev Returns the current nonce for `owner`. This value must be
     * included whenever a signature is generated for {permit}.
     *
     * Every successful call to {permit} increases ``owner``'s nonce by one. This
     * prevents a signature from being used multiple times.
     */
    function nonces(address owner) public view returns (uint256) {
        return _nonces[owner].current();
    }

    /**
     * @dev Returns the domain separator used in the encoding of the signature for {permit}, as defined by {EIP712}.
     */
    // solhint-disable-next-line func-name-mixedcase
    function DOMAIN_SEPARATOR() external view returns (bytes32) {
        return _domainSeparatorV4();
    }

    /**
     * @dev "Consume a nonce": return the current value and increment.
     *
     * _Available since v4.1._
     */
    function _useNonce(address owner) internal virtual returns (uint256 current) {
        CountersUpgradeable.Counter storage nonce = _nonces[owner];
        current = nonce.current();
        nonce.increment();
    }
}

