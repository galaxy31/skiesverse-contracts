//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/utils/cryptography/EIP712.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "./Nonces.sol";

contract Adaran is ERC20("Adaran","ADR"), AccessControl, EIP712("Adaran","1"), Nonces{

    bytes32 public constant SIGNER = keccak256("SIGNER");
    // solhint-disable-next-line var-name-mixedcase
    bytes32 private constant _PERMIT_TYPEHASH =
    keccak256("PermitMint(address owner,uint256 value,uint256 nonce)");
    
    constructor(){
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(SIGNER, msg.sender);
    }

    /**
     * @dev Mints for `to` `value` tokens,
     * given ``owner``'s signed approval.
     *
     *
     * Emits an {Transfer} event.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     * - `value` cannot be the zero amount.
     * - `v`, `r` and `s` must be a valid `secp256k1` signature from `owner`
     * over the EIP712-formatted function arguments.
     * - the signature must use ``owner``'s current nonce (see {nonces}).
     *
     * For more information on the signature format, see the
     * https://eips.ethereum.org/EIPS/eip-2612#specification[relevant EIP
     * section].
     */
    function permitMint(
        address to,
        uint256 value,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external virtual {
        bytes32 structHash = keccak256(abi.encode(_PERMIT_TYPEHASH, to, value, _useNonce(to)));
        bytes32 hash = _hashTypedDataV4(structHash);
        address signer = ECDSA.recover(hash, v, r, s);
        require(hasRole(SIGNER, signer), "ERC20Permit: invalid signature");
        _mint(to, value);
    }
    
}