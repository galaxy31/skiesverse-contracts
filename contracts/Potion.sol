//SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/utils/cryptography/EIP712.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "./Nonces.sol";

contract Potion is ERC1155, AccessControl,  EIP712("Potion","1"), Nonces{
    bytes32 public constant SIGNER = keccak256("SIGNER");
    address private immutable _token;
    address private immutable _pool;
    string public name="Potion";
    string public symbol="PTN";
    string private _baseTokenURI;
    // solhint-disable-next-line var-name-mixedcase
    bytes32 private constant _PERMIT_TYPEHASH =
    keccak256("PermitMint(address owner,uint256 tokenId,uint256 amount,uint256 value,uint256 nonce)");

    /**
     * @dev initialize contract with base tokens `token` as Skies and token receiver as  `pool`.
     *
     */
    constructor(address token,address pool) ERC1155(""){
        require(token!=address(0),"Invalid token address");
        require(pool!=address(0),"Invalid pool address");
        _token=token;
        _pool=pool;
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(SIGNER, msg.sender);
    }

    /**
     * @dev Return token's URI=base+ `tokenId` + '.json'.
     *
     */

    function uri(uint256 tokenId) public view virtual override returns (string memory) {
        return  string(abi.encodePacked(_baseTokenURI,Strings.toString(tokenId), ".json" ));
    }

    /**
     * @dev Return `_baseTokenURI` as the base for token's URI.
     *
     */

    function _baseURI() internal view virtual returns (string memory) {
        return  _baseTokenURI;
    }
    /**
     * @dev Sets `_baseTokenURI` as the base for token's URI.
     *
     */
    function setBaseURI(string memory url) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _baseTokenURI=url;
    }
    
    /**
     * @dev Mints for `to` `tokenId` token in `amount` for `value` tokens,
     * given ``signer``'s signed approval.
     *
     *
     * Emits an {Transfer} event.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     * - `v`, `r` and `s` must be a valid `secp256k1` signature from `owner`
     * over the EIP712-formatted function arguments.
     * - the signature must use ``owner``'s current nonce (see {nonces}).
     *
     * For more information on the signature format, see the
     * https://eips.ethereum.org/EIPS/eip-2612#specification[relevant EIP
     * section].
     */
    function permitMint(
        address to,
        uint256 tokenId,
        uint256 amount,
        uint256 value,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external virtual {
        bytes32 structHash = keccak256(abi.encode(_PERMIT_TYPEHASH, to, tokenId,amount,value, _useNonce(to)));
        bytes32 hash = _hashTypedDataV4(structHash);
        address signer = ECDSA.recover(hash, v, r, s);
        require(hasRole(SIGNER, signer), "mintPermit: invalid signature");
        SafeERC20.safeTransferFrom(IERC20(_token), _msgSender(),_pool,value);
        _mint(to, tokenId, amount, '');
    }
    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override(AccessControl,ERC1155)  returns (bool) {
        return super.supportsInterface(interfaceId);
    }


}