//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

error IllegalArrayData();
error IllegalAmountData();
error IllegalOwnerData();

contract Skies is ERC20("Skies","SKI"){
    constructor(address[] memory owners, uint256[] memory amounts ){
        if (owners.length != amounts.length) revert IllegalArrayData();
        for(uint256 i; i<owners.length;){
            if (address(0)==owners[i]) revert IllegalOwnerData();
            if (0==amounts[i]) revert IllegalAmountData();
            _mint(owners[i], amounts[i]);
            unchecked{
                i++;
            }
        }
    }
}