// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

// Import this file to use console.log
import "hardhat/console.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/security/PullPayment.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract Skies is ERC20, PullPayment, Ownable {
    event Bought(
        address indexed recipient,
        uint amount
    );

    constructor(uint256 initialSupply) ERC20("Skies", "SKI") {
        _mint(address(this), initialSupply);
    }

    function mint(uint amount) onlyOwner external {
        _mint(address(this), amount);
    }

    function transferTo(address recipient, uint amount) onlyOwner external {
        _transfer(address(this), recipient, amount);
    }

    receive() external payable {
        uint tokensToBuy = msg.value;
        require(tokensToBuy > 0, Strings.toString(msg.value));

        uint currentBalance = balanceOf(address(this));
        require(tokensToBuy <= currentBalance, "not enought tokens!");

        _transfer(address(this), msg.sender, tokensToBuy);
        emit Bought(msg.sender, tokensToBuy);
    }

    function withdrawMoneyToOwner() public onlyOwner {
        payable(owner()).transfer(address(this).balance);
    }
}
