// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/security/PullPaymentUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/ERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";

contract GameItems is ERC1155Upgradeable, PullPaymentUpgradeable, OwnableUpgradeable {
    ERC20Upgradeable private skies;

    uint256 public constant LEVEL_UP_HERO_1_40 = 1;
    uint256 public constant LEVEL_UP_HERO_41_60 = 2;
    uint256 public constant LEVEL_UP_HERO_61_80 = 3;

    event mintItemEvent(
        address indexed recipient,
        uint256 itemId,
        uint256 count
    );
    function initialize() initializer public {
        //_mint(msg.sender, LEVEL_UP_HERO_1_40, 10 ** 10, "");
        //_mint(msg.sender, LEVEL_UP_HERO_41_60, 10 ** 5, "");
        //_mint(msg.sender, LEVEL_UP_HERO_61_80, 10 ** 3, "");
        __ERC1155_init("https://skiesverse.galaxy4g.com/api/metadata/items/");
        __PullPayment_init();
        __Ownable_init();
    }

    function burn(uint256 tokenId, uint amount) public {
        require(amount > 0, "amount < 0");
        require(balanceOf(msg.sender, tokenId) >= amount, "balance of msg.sender for that tokenId < amount ");
        _burn(msg.sender, tokenId, amount);
    }

    function setSkies(address payable _skies) public onlyOwner {
        skies = ERC20Upgradeable(_skies);
    }

    function mintItemTo(address recipient, uint256 item, uint count) public onlyOwner {
        _mint(recipient, item, count, "");
        emit mintItemEvent(recipient, item, count);
    }

    /// @dev Sets the base token URI prefix.
    function setBaseTokenURI(string memory uri_) external onlyOwner {
        _setURI(uri_);
    }

    function withdrawPayments(address payable payee) public override onlyOwner virtual {
        super.withdrawPayments(payee);
    }

    /**
   * Override isApprovedForAll to auto-approve OS's proxy contract
   */
    function isApprovedForAll(
        address _owner,
        address _operator
    ) public override view returns (bool isOperator) {
        // if OpenSea's ERC1155 Proxy Address is detected, auto-return true
        // for Polygon's Mumbai testnet, use 0x53d791f18155C211FF8b58671d0f7E9b50E596ad
        if (_operator == address(0x207Fa8Df3a17D96Ca7EA4f2893fcdCb78a304101)) {
            return true;
        }
        // otherwise, use the default ERC1155.isApprovedForAll()
        return ERC1155Upgradeable.isApprovedForAll(_owner, _operator);
    }
}
