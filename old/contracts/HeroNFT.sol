// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./NFT.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "hardhat/console.sol";

contract HeroNFT is Initializable, NFT {
    ERC20 private skies;
    ERC1155 private gameItems;
    event Upgrade(address indexed sender, uint256 tokenId, uint level);
    event MintHero(address indexed recipient, uint tokenId);


    function initialize() initializer public {
        __InitNft("Skiesverse-heroes", "SKI-H");
    }

    function setSkies(address payable _skies) external onlyOwner {
        skies = ERC20(_skies);
    }

    function setGameItems(address payable _gameItems) external onlyOwner {
        gameItems = ERC1155(_gameItems);
    }

    function mintHero(uint tokenId) public {
        uint skiesPrice = 5;
        skies.transferFrom(_msgSender(), address(skies), skiesPrice);

        safeMintInternal(_msgSender(), tokenId);
        emit MintHero(_msgSender(), tokenId);
    }

    function upgrade(uint256 tokenId, uint level) public {
        require(ownerOf(tokenId) == _msgSender(), "Not yours token");

        uint skiesPrice = 1;
        require(skies.balanceOf(_msgSender()) >= skiesPrice, "Not enough skiesToken ");
        skies.transferFrom(_msgSender(), address(skies), skiesPrice);

        emit Upgrade(_msgSender(), tokenId, level);
    }

    function mintHeroesTo(address recipient, uint[] memory ids) external onlyOwner  {
        internalBatchedMintTo(recipient, ids);

        for (uint i = 0; i < ids.length; i++) {
            emit MintHero(recipient, ids[i]);
        }
    }

    function burnTokens(uint[] memory ids) external onlyOwner  {
        internalBatchedBurn(ids);
    }
}