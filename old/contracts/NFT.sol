// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721EnumerableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "./libs/ContextMixin.sol";
import "hardhat/console.sol";

contract NFT is Initializable, ERC721EnumerableUpgradeable,  ContextMixin, OwnableUpgradeable {
    string public baseTokenURI;
    address private addressApprovedForAll;
    uint256[19] __gap;

    function __InitNft(string memory bigName, string memory smlName) internal onlyInitializing{
        __ERC721_init(bigName, smlName);
        __Ownable_init();

        baseTokenURI = "";
        addressApprovedForAll = 0xff7Ca10aF37178BdD056628eF42fD7F799fAc77c;
        console.log(_msgSender());
    }

    function setAddressApprovedForAll(address _addressApprovedForAll) external onlyOwner {
        addressApprovedForAll = _addressApprovedForAll;
    }

    function safeMintInternal(address recipient, uint id) internal{
        _safeMint(recipient, id);
    }

    function internalBatchedMintTo(address recipient, uint[] memory ids) internal{
        for (uint i = 0; i < ids.length; i++) {
            _safeMint(recipient, ids[i]);
        }
    }

    function internalBatchedBurn(uint[] memory ids) internal{
        for (uint i = 0; i < ids.length; i++) {
            _burn(ids[i]);
        }
    }

    /// @dev Returns an URI for a given token ID
    function _baseURI() internal view virtual override returns (string memory) {
        return baseTokenURI;
    }

    /// @dev Sets the base token URI prefix.
    function setBaseTokenURI(string memory _baseTokenURI) public onlyOwner {
        baseTokenURI = _baseTokenURI;
    }

    /**
     * This is used instead of msg.sender as transactions won't be sent by the original token owner, but by OpenSea.
     */
    function _msgSender() internal override view returns (address sender)
    {
        return ContextMixin.msgSender();
    }

    /**
   * Override isApprovedForAll to auto-approve OS's proxy contract
   */
    function isApprovedForAll(
        address _owner,
        address _operator
    ) public override view returns (bool isOperator) {
        // if OpenSea's ERC721 Proxy Address is detected, auto-return true
        // for Polygon's Mumbai testnet, use 0xff7Ca10aF37178BdD056628eF42fD7F799fAc77c
        //if (_operator == address(0x58807baD0B376efc12F5AD86aAc70E78ed67deaE)) {
        if (_operator == addressApprovedForAll) {
            return true;
        }

        // otherwise, use the default ERC721.isApprovedForAll()
        return ERC721Upgradeable.isApprovedForAll(_owner, _operator);
    }
}