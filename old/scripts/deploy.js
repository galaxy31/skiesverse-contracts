// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
const hre = require("hardhat");
const {string} = require("hardhat/internal/core/params/argumentTypes");

async function main() {
  // Get our account (as deployer) to verify that a minimum wallet balance is available
  const [deployer] = await hre.ethers.getSigners();

  console.log(`Deploying contracts with the account: ${deployer.address}`);
  console.log(`Account balance: ${(await deployer.getBalance()).toString()}`);

  /*const Skies = await hre.ethers.getContractFactory("Skies");
  const skies = await Skies.deploy(100);
  await skies.deployed();
  console.log("Skies deployed to:", skies.address);
  */
  /*
  const GameItems = await hre.ethers.getContractFactory("GameItems");
  const gameItems = await GameItems.deploy(skies.address);
  await gameItems.deployed();
  console.log("GameItems deployed to:", gameItems.address);
  */

  const HeroNft = await hre.ethers.getContractFactory("HeroNFT");
  const heroNft = await hre.upgrades.deployProxy(HeroNft);
  await heroNft.deployed();
  console.log("HeroNft deployed to:", heroNft.address);

  console.log(`Account balance: ${(await deployer.getBalance()).toString()}`);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
