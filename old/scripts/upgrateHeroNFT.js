// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
const hre = require("hardhat");
const {string} = require("hardhat/internal/core/params/argumentTypes");

async function main() {
  // Get our account (as deployer) to verify that a minimum wallet balance is available
  const [deployer] = await hre.ethers.getSigners();

  console.log(`Deploying contracts with the account: ${deployer.address}`);
  console.log(`Account balance: ${(await deployer.getBalance()).toString()}`);

  const HeroNft = await hre.ethers.getContractFactory("HeroNFT");
  const heroNft = await hre.upgrades.upgradeProxy("0x2DfB8a89E4f50596d0301767C3d9b8E02aBbbeCF", HeroNft);
  console.log("HeroNft upgradeProxy to:", heroNft.address);
  console.log(`Account balance: ${(await deployer.getBalance()).toString()}`);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
