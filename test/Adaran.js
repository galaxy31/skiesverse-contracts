const { expect } = require("chai");
const  hre  = require('hardhat');

const CHAIN_ID=31337

describe("Adaran", function () {
let adaran;
    it("Deploy", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const ADARAN = await ethers.getContractFactory("Adaran");
      adaran = await ADARAN.deploy();
    });

    it('has a name', async function () {
      expect(await adaran.name()).to.equal('Adaran');
    });

    it('has a symbol', async function () {
      expect(await adaran.symbol()).to.equal('ADR');
    });

    it('has 18 decimals', async function () {
      expect(await adaran.decimals()).to.equal(18);
    });

    it("Mint with permit to owner", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      //PermitMint(address owner,uint256 value,uint256 nonce)
      const value="1000000000000000000000000000"
      const nonce=(await adaran.nonces(owner.address)).toString()
      const msgParams = JSON.stringify({
    domain: {
      chainId: CHAIN_ID,
      name: 'Adaran',
      verifyingContract: adaran.address,
      version: '1',
    },

    message: {
      owner: owner.address,
      value: value,
      nonce: nonce
    },
    primaryType: 'PermitMint',
    types: {
      EIP712Domain: [
        { name: 'name', type: 'string' },
        { name: 'version', type: 'string' },
        { name: 'chainId', type: 'uint256' },
        { name: 'verifyingContract', type: 'address' },
      ],
      PermitMint: [
        { name: 'owner', type: 'address' },
        { name: 'value', type: 'uint256' },
        { name: 'nonce', type: 'uint256' }
      ]
    },
  })

    const signature=await hre.network.provider.request({
      method: "eth_signTypedData_v4",
      params: [owner.address,msgParams],
    });
    const r = signature.substr(0,66)
    const s = "0x" + signature.substr(66,64)
    const v = Number("0x" + signature.substr(-2))
    await adaran.permitMint(owner.address, value, v, r, s)
      
      expect( await adaran.balanceOf(owner.address)).to.equal(value) ;
    });


    it("Get totalSupply", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      expect( await adaran.totalSupply()).to.equal("1000000000000000000000000000") ;
    });

    it("Transfer from owner to other account", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await adaran.transfer(otherAccount.address,"500000000000000000000000000");
      expect( await adaran.balanceOf(otherAccount.address)).to.equal("500000000000000000000000000");
    });

    it("Approve from owner to other account", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await adaran.approve(otherAccount.address,"500000000000000000000000000");
      expect( await adaran.allowance(owner.address,otherAccount.address)).to.equal("500000000000000000000000000");
    });

    it("TransferFrom from owner to other account", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await adaran.connect(otherAccount).transferFrom(owner.address,otherAccount.address,"500000000000000000000000000");
      expect( await adaran.balanceOf(otherAccount.address)).to.equal("1000000000000000000000000000");
    });

    it('reverts transfer from zero balance', async function () {
      const [owner, otherAccount] = await ethers.getSigners();
        await expect(
          adaran.transfer(otherAccount.address,"500000000000000000000000000" ),
        ).to.be.revertedWith('ERC20: transfer amount exceeds balance');
      });

    it('reverts decreaseAllowance from zero balance', async function () {
      const [owner, otherAccount] = await ethers.getSigners();
        await expect(
          adaran.decreaseAllowance(otherAccount.address,"500000000000000000000000000" ),
        ).to.be.revertedWith('ERC20: decreased allowance below zero');
      });
});
