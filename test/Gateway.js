const  hre  = require('hardhat');
const { expect } = require("chai");
const { time } = require("@nomicfoundation/hardhat-network-helpers");


const CHAIN_ID=31337

describe("Gateway", function () {
  let adaran
  let skies
  let gateway
  let hero

    it("Deploy skies", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const SKIES = await ethers.getContractFactory("Skies");
      skies = await SKIES.deploy([owner.address], ["10000000000000000000000000000000"]);
    });

    it("Deploy adaran", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const ADARAN = await ethers.getContractFactory("Adaran");
      adaran = await ADARAN.deploy();
    });

    it("Deploy Gateway", async function () {
      const Gateway = await ethers.getContractFactory("PoolGateway");
      gateway = await upgrades.deployProxy(Gateway, [skies.address, adaran.address]);
    });

    it("Upgrade Gateway", async function () {
      const Gateway2 = await ethers.getContractFactory("PoolGateway");
      gateway = await upgrades.upgradeProxy(gateway.address, Gateway2);
    });
  
    it("Deploy Hero", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const HERO = await ethers.getContractFactory("Hero");
      hero = await HERO.deploy(skies.address,gateway.address);
    });


    it("Mint token 0 with permit to owner for 1 ether", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const value="1000000000000000000"
      const nonce=(await hero.nonces(owner.address)).toString()
      const msgParams = JSON.stringify({
    domain: {
      chainId: CHAIN_ID,
      name: 'Hero',
      verifyingContract: hero.address,
      version: '1',
    },

    message: {
      owner: owner.address,
      tokenId: 0,
      value: value,
      nonce: nonce
    },
    primaryType: 'PermitMintEther',
    types: {
      EIP712Domain: [
        { name: 'name', type: 'string' },
        { name: 'version', type: 'string' },
        { name: 'chainId', type: 'uint256' },
        { name: 'verifyingContract', type: 'address' },
      ],
      PermitMintEther: [
        { name: 'owner', type: 'address' },
        { name: 'tokenId', type: 'uint256' },
        { name: 'value', type: 'uint256' },
        { name: 'nonce', type: 'uint256' }
      ]
    },
  })

    const signature=await hre.network.provider.request({
      method: "eth_signTypedData_v4",
      params: [owner.address,msgParams],
    });
    const r = signature.substr(0,66)
    const s = "0x" + signature.substr(66,64)
    const v = Number("0x" + signature.substr(-2))
    const tx=await hero.permitMintForEthers(owner.address, 0, value, v, r, s,{ value: value })
    //const receipt = await tx.wait()
    //for (const event of receipt.events) {
    //    console.log(`Event ${event.event} with args ${event.args}`);
    //}

      expect( await hero.ownerOf("0")).to.equal(owner.address) ;
    });



    it("Mint with permit to owner", async function () {
      const [owner, addr1] = await ethers.getSigners();
      //PermitMint(address owner,uint256 value,uint256 nonce)
      const value="1000000000000000000000000000"
      const nonce=(await adaran.nonces(owner.address)).toString()
//===================  BACKEND starts
    const msgParams = JSON.stringify({
    domain: {
      chainId: CHAIN_ID,
      name: 'Adaran',
      verifyingContract: adaran.address,
      version: '1',
    },

    message: {
      owner: owner.address,
      value: value,
      nonce: nonce
    },
    primaryType: 'PermitMint',
    types: {
      EIP712Domain: [
        { name: 'name', type: 'string' },
        { name: 'version', type: 'string' },
        { name: 'chainId', type: 'uint256' },
        { name: 'verifyingContract', type: 'address' },
      ],
      PermitMint: [
        { name: 'owner', type: 'address' },
        { name: 'value', type: 'uint256' },
        { name: 'nonce', type: 'uint256' }
      ]
    },
  })

    const signature=await hre.network.provider.request({
      method: "eth_signTypedData_v4",
      params: [owner.address,msgParams],
    });
    const r = signature.substr(0,66)
    const s = "0x" + signature.substr(66,64)
    const v = Number("0x" + signature.substr(-2))
//===================  BACKEND ends
//==================== FRONTEND starts
    await adaran.permitMint(owner.address, value, v, r, s)
//==================== FRONTEND ends
      expect( await adaran.balanceOf(owner.address)).to.equal(value) ;
    });

    it("Give signer role to account2", async function () {
      const [owner] = await ethers.getSigners();
      const dev_role= await gateway.DEVELOPER() 
      const signer_role= await gateway.SIGNER() 
      await gateway.grantRole( signer_role, owner.address)
      expect( await gateway.hasRole( signer_role, owner.address)).to.equal(true) ;
 
    });

    it("Transfer skies from owner to addr1", async function () {
      const [owner,addr1] = await ethers.getSigners();
      const value="100000000000000000000000000"
      await skies.transfer( addr1.address, value)
      expect( await skies.balanceOf( addr1.address)).to.equal(value) ;
 
    });


    it("Spend skies tokens from addr1", async function () {
      const [owner, addr1] = await ethers.getSigners();
      const value="10000000000000000000000000"
      const nonce=(await gateway.nonces(addr1.address)).toString()
      const msgParams = JSON.stringify({
    domain: {
      chainId: CHAIN_ID,
      name: 'PoolGateway',
      verifyingContract: gateway.address,
      version: '1',
    },

    message: {
      from: addr1.address,
      token: skies.address,
      value: value,
      eventId: "1",
      nonce: nonce
    },
    primaryType: 'spendTokens',
    types: {
      EIP712Domain: [
        { name: 'name', type: 'string' },
        { name: 'version', type: 'string' },
        { name: 'chainId', type: 'uint256' },
        { name: 'verifyingContract', type: 'address' },
      ],
      spendTokens: [
        { name: 'from', type: 'address' },
        { name: 'token', type: 'address' },
        { name: 'value', type: 'uint256' },
        { name: 'eventId', type: 'uint256' },
        { name: 'nonce', type: 'uint256' }
      ]
    },
  })

    const signature=await hre.network.provider.request({
      method: "eth_signTypedData_v4",
      params: [owner.address,msgParams],
    });
    const r = signature.substr(0,66)
    const s = "0x" + signature.substr(66,64)
    const v = Number("0x" + signature.substr(-2))
    await skies.connect(addr1).approve(gateway.address, value)
    await gateway.connect(addr1).spendTokens(addr1.address, skies.address, value, "1" , v, r, s)
      
      expect( await skies.balanceOf(gateway.address)).to.equal(value);
    });



    it("Add stake 6% for 90days addr1", async function () {
      const [owner, addr1] = await ethers.getSigners();
      const amount="100000000000000000000000"
      const reward=6 
      const start=Math.round(Date.now() / 1000)
      const end=Math.round(Date.now() / 1000) + 42 * 60 * 60 * 90
      const nonce=(await gateway.nonces(addr1.address)).toString()

      const msgParams = JSON.stringify({
    domain: {
      chainId: CHAIN_ID,
      name: 'PoolGateway',
      verifyingContract: gateway.address,
      version: '1',
    },

    message: {
      to: addr1.address,
      start: start,
      end: end,
      reward: reward,
      amount: amount,
      nonce: nonce
    },
    primaryType: 'addStake',
    types: {
      EIP712Domain: [
        { name: 'name', type: 'string' },
        { name: 'version', type: 'string' },
        { name: 'chainId', type: 'uint256' },
        { name: 'verifyingContract', type: 'address' },
      ],
      addStake: [
        { name: 'to', type: 'address' },
        { name: 'start', type: 'uint40' },
        { name: 'end', type: 'uint40' },
        { name: 'reward', type: 'uint8' },
        { name: 'amount', type: 'uint256' },
        { name: 'nonce', type: 'uint256' }
      ]
    },
  })

    const signature=await hre.network.provider.request({
      method: "eth_signTypedData_v4",
      params: [owner.address,msgParams],
    });
    const r = signature.substr(0,66)
    const s = "0x" + signature.substr(66,64)
    const v = Number("0x" + signature.substr(-2))

    await skies.connect(addr1).approve(gateway.address, amount)
    await gateway.connect(addr1).addStake(addr1.address, start, end, reward, amount, v, r, s)
    const stake=await gateway.stakes(addr1.address,0)
      expect( stake["amount"]).to.equal(amount);
    });



    it("Get stake after 90days for addr1", async function () {
      const [owner, addr1] = await ethers.getSigners();
      const current = await time.latest()
      await time.increaseTo(current + 24 * 60 * 60 * 90);
      await gateway.connect(addr1).finishStake(0)
      expect( await skies.balanceOf(addr1.address)).to.equal("90006000000000000000000000");
    });


    it("Withdraw ethers for developers", async function () {
      const [owner, addr1] = await ethers.getSigners();
      const balance=await hre.ethers.provider.getBalance(gateway.address)
      await gateway.grantRole("0x2714cbbaddbb71bcae9366d8bf7770636ec7ae63227b573986d2f54fffacb39d",addr1.address)
      await gateway.connect(addr1).withdrawEthers("1000000000000")
    });

    it("Withdraw skies for developers", async function () {
      const [owner, addr1] = await ethers.getSigners();
      await gateway.grantRole("0x2714cbbaddbb71bcae9366d8bf7770636ec7ae63227b573986d2f54fffacb39d",addr1.address)
      await gateway.connect(addr1).withdrawSkies("1000000000000")
      expect( await skies.balanceOf(addr1.address)).to.equal("90006000000001000000000000");
    });





});
