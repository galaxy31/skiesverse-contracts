const { expect } = require("chai");
const  hre  = require('hardhat');

const CHAIN_ID=31337

describe("Potion", function () {
let potion, skies, adaran, gateway;

    it("Deploy skies", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const SKIES = await ethers.getContractFactory("Skies");
      skies = await SKIES.deploy([owner.address], ["1000000000000000000000000000"]);
    });

    it("Deploy adaran", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const ADARAN = await ethers.getContractFactory("Adaran");
      adaran = await ADARAN.deploy();
    });


    it("Deploy Gateway", async function () {
      const Gateway = await ethers.getContractFactory("PoolGateway");
      gateway = await upgrades.deployProxy(Gateway, [skies.address, adaran.address]);
    });

    it("Deploy", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const POTION = await ethers.getContractFactory("Potion");
      potion = await POTION.deploy(skies.address,gateway.address);
    });

    it('has a name', async function () {
      expect(await potion.name()).to.equal('Potion');
    });

    it('has a symbol', async function () {
      expect(await potion.symbol()).to.equal('PTN');
    });


    it("Mint token 1 with permit to owner for 1 Skies", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const value="1000000000000000000"
      const nonce=(await potion.nonces(owner.address)).toString()
      const msgParams = JSON.stringify({
    domain: {
      chainId: CHAIN_ID,
      name: 'Potion',
      verifyingContract: potion.address,
      version: '1',
    },

    message: {
      owner: owner.address,
      tokenId: "1",
      amount: "100",
      value: value,
      nonce: nonce
    },
    primaryType: 'PermitMint',
    types: {
      EIP712Domain: [
        { name: 'name', type: 'string' },
        { name: 'version', type: 'string' },
        { name: 'chainId', type: 'uint256' },
        { name: 'verifyingContract', type: 'address' },
      ],
      PermitMint: [
        { name: 'owner', type: 'address' },
        { name: 'tokenId', type: 'uint256' },
        { name: 'amount', type: 'uint256' },
        { name: 'value', type: 'uint256' },
        { name: 'nonce', type: 'uint256' }
      ]
    },
  })

    const signature=await hre.network.provider.request({
      method: "eth_signTypedData_v4",
      params: [owner.address,msgParams],
    });
    const r = signature.substr(0,66)
    const s = "0x" + signature.substr(66,64)
    const v = Number("0x" + signature.substr(-2))

    await skies.approve(potion.address, value)
    await potion.permitMint(owner.address, 1, 100, value, v, r, s)
      
      expect( await potion.balanceOf(owner.address,"1")).to.equal(100)
    });



    it("Set base URI", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await potion.setBaseURI("https://backend/");
      expect( await potion.uri("1")).to.equal("https://backend/1.json");
    });



    it("Transfer from owner to other account", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await potion.safeTransferFrom(owner.address,otherAccount.address,1,50,"0x");
      expect( await potion.balanceOf(otherAccount.address,"1")).to.equal("50");
    });

    it("Approve from owner to other account", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await potion.setApprovalForAll(otherAccount.address,true);
      expect( await potion.isApprovedForAll(owner.address,otherAccount.address)).to.equal(true);
    });

    it("TransferFrom from owner to other account", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await potion.connect(otherAccount).safeTransferFrom(owner.address,otherAccount.address,"1","25","0x");
      expect( await potion.balanceOf(otherAccount.address,"1")).to.equal(75);
    });

    it('reverts transfer from zero balance', async function () {
      const [owner, otherAccount] = await ethers.getSigners();
        await expect(
          potion.safeTransferFrom(owner.address, otherAccount.address,"1",50,"0x" ),
        ).to.be.revertedWith('ERC1155: insufficient balance for transfer');
      });
});
