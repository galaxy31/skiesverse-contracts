const { expect } = require("chai");
const  hre  = require('hardhat');

const CHAIN_ID=31337

describe("Hero", function () {
let hero, skies, adaran, gateway;

    it("Deploy skies", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const SKIES = await ethers.getContractFactory("Skies");
      skies = await SKIES.deploy([owner.address], ["1000000000000000000000000000"]);
    });

    it("Deploy adaran", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const ADARAN = await ethers.getContractFactory("Adaran");
      adaran = await ADARAN.deploy();
    });


    it("Deploy Gateway", async function () {
      const Gateway = await ethers.getContractFactory("PoolGateway");
      gateway = await upgrades.deployProxy(Gateway, [skies.address, adaran.address]);
    });

    it("Deploy hero", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const HERO = await ethers.getContractFactory("Hero");
      hero = await HERO.deploy(skies.address,gateway.address);
    });

    it('has a name', async function () {
      expect(await hero.name()).to.equal('Hero');
    });

    it('has a symbol', async function () {
      expect(await hero.symbol()).to.equal('HRO');
    });

    it("Mint token 0 with permit to owner for 1 ether", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const value="1000000000000000000"
      const nonce=(await hero.nonces(owner.address)).toString()
      const msgParams = JSON.stringify({
    domain: {
      chainId: CHAIN_ID,
      name: 'Hero',
      verifyingContract: hero.address,
      version: '1',
    },

    message: {
      owner: owner.address,
      tokenId: 0,
      value: value,
      nonce: nonce
    },
    primaryType: 'PermitMintEther',
    types: {
      EIP712Domain: [
        { name: 'name', type: 'string' },
        { name: 'version', type: 'string' },
        { name: 'chainId', type: 'uint256' },
        { name: 'verifyingContract', type: 'address' },
      ],
      PermitMintEther: [
        { name: 'owner', type: 'address' },
        { name: 'tokenId', type: 'uint256' },
        { name: 'value', type: 'uint256' },
        { name: 'nonce', type: 'uint256' }
      ]
    },
  })

    const signature=await hre.network.provider.request({
      method: "eth_signTypedData_v4",
      params: [owner.address,msgParams],
    });
    const r = signature.substr(0,66)
    const s = "0x" + signature.substr(66,64)
    const v = Number("0x" + signature.substr(-2))
    const tx=await hero.permitMintForEthers(owner.address, 0, value, v, r, s,{ value: value })
    //const receipt = await tx.wait()
    //for (const event of receipt.events) {
    //    console.log(`Event ${event.event} with args ${event.args}`);
    //}

      expect( await hero.ownerOf("0")).to.equal(owner.address) ;
    });


    it("Mint token 1 with permit to owner for 1 Skies", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const value="1000000000000000000"
      const nonce=(await hero.nonces(owner.address)).toString()
      const msgParams = JSON.stringify({
    domain: {
      chainId: CHAIN_ID,
      name: 'Hero',
      verifyingContract: hero.address,
      version: '1',
    },

    message: {
      owner: owner.address,
      tokenId: 1,
      value: value,
      nonce: nonce
    },
    primaryType: 'PermitMint',
    types: {
      EIP712Domain: [
        { name: 'name', type: 'string' },
        { name: 'version', type: 'string' },
        { name: 'chainId', type: 'uint256' },
        { name: 'verifyingContract', type: 'address' },
      ],
      PermitMint: [
        { name: 'owner', type: 'address' },
        { name: 'tokenId', type: 'uint256' },
        { name: 'value', type: 'uint256' },
        { name: 'nonce', type: 'uint256' }
      ]
    },
  })

    const signature=await hre.network.provider.request({
      method: "eth_signTypedData_v4",
      params: [owner.address,msgParams],
    });
    const r = signature.substr(0,66)
    const s = "0x" + signature.substr(66,64)
    const v = Number("0x" + signature.substr(-2))

    await skies.approve(hero.address, value)
    await hero.permitMint(owner.address, 1, value, v, r, s)
      
      expect( await hero.ownerOf("1")).to.equal(owner.address) ;
    });



    it("Set base URI", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await hero.setBaseURI("https://backend/");
      expect( await hero.tokenURI("0")).to.equal("https://backend/0.json");
    });



    it("Get count of owner's tokens", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      expect( await hero.balanceOf(owner.address)).to.equal("2") ;
    });

    it("Transfer from owner to other account", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await hero.transferFrom(owner.address,otherAccount.address,"0");
      expect( await hero.balanceOf(otherAccount.address)).to.equal("1");
      expect( await hero.ownerOf("0")).to.equal(otherAccount.address);
    });

    it("Approve from owner to other account", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await hero.approve(otherAccount.address,"1");
      expect( await hero.getApproved("1")).to.equal(otherAccount.address);
    });

    it("TransferFrom from owner to other account", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      await hero.connect(otherAccount).transferFrom(owner.address,otherAccount.address,"1");
      expect( await hero.ownerOf("1")).to.equal(otherAccount.address);
    });

    it('reverts transfer from zero balance', async function () {
      const [owner, otherAccount] = await ethers.getSigners();
        await expect(
          hero.transferFrom(owner.address, otherAccount.address,"5" ),
        ).to.be.revertedWith('ERC721: invalid token ID');
      });
});
