const { expect } = require("chai");
const  hre  = require('hardhat');
const ethUtil = require('ethereumjs-util');
const abi = require('ethereumjs-abi');

const CHAIN_ID=31337
let types,typedData

function dependencies(primaryType, found = []) {
    if (found.includes(primaryType)) {
        return found;
    }
    if (types[primaryType] === undefined) {
        return found;
    }
    found.push(primaryType);
    for (let field of types[primaryType]) {
        for (let dep of dependencies(field.type, found)) {
            if (!found.includes(dep)) {
                found.push(dep);
            }
        }
    }
    return found;
}

function encodeType(primaryType) {
    // Get dependencies primary first, then alphabetical
    let deps = dependencies(primaryType);
    deps = deps.filter(t => t != primaryType);
    deps = [primaryType].concat(deps.sort());

    // Format as a string with fields
    let result = '';
    for (let type of deps) {
        result += `${type}(${types[type].map(({ name, type }) => `${type} ${name}`).join(',')})`;
    }
    return result;
}

function typeHash(primaryType) {
    return ethUtil.keccakFromString(encodeType(primaryType), 256);
}

function encodeData(primaryType, data) {
    let encTypes = [];
    let encValues = [];

    // Add typehash
    encTypes.push('bytes32');
    encValues.push(typeHash(primaryType));

    // Add field contents
    for (let field of types[primaryType]) {
        let value = data[field.name];
        if (field.type == 'string' || field.type == 'bytes') {
            encTypes.push('bytes32');
            value = ethUtil.keccakFromString(value, 256);
            encValues.push(value);
        } else if (types[field.type] !== undefined) {
            encTypes.push('bytes32');
            value = ethUtil.keccak256(encodeData(field.type, value));
            encValues.push(value);
        } else if (field.type.lastIndexOf(']') === field.type.length - 1) {
            throw 'TODO: Arrays currently unimplemented in encodeData';
        } else {
            encTypes.push(field.type);
            encValues.push(value);
        }
    }

    return abi.rawEncode(encTypes, encValues);
}


function structHash(primaryType, data) {
    return ethUtil.keccak256(encodeData(primaryType, data));
}

function signHash() {
    return ethUtil.keccak256(
        Buffer.concat([
            Buffer.from('1901', 'hex'),
            structHash('EIP712Domain', typedData.domain),
            structHash(typedData.primaryType, typedData.message),
        ]),
    );
}

describe("Armor with ether utils sign", function () {
let armor, skies, adaran, gateway;

    it("Deploy skies", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const SKIES = await ethers.getContractFactory("Skies");
      skies = await SKIES.deploy([owner.address], ["1000000000000000000000000000"]);
    });

    it("Deploy adaran", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const ADARAN = await ethers.getContractFactory("Adaran");
      adaran = await ADARAN.deploy();
    });


    it("Deploy Gateway", async function () {
      const Gateway = await ethers.getContractFactory("PoolGateway");
      gateway = await upgrades.deployProxy(Gateway, [skies.address, adaran.address]);
    });

    it("Deploy", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const ARMOR = await ethers.getContractFactory("Armor");
      armor = await ARMOR.deploy(skies.address,gateway.address);
    });



    it("Sign with ethers utils without node and mint permit for 1 ether", async function () {
      const [owner, otherAccount] = await ethers.getSigners();
      const value="1000000000000000000"
      const nonce=(await armor.nonces(owner.address)).toString()

    typedData ={
    domain: {
      chainId: CHAIN_ID,
      name: 'Armor',
      verifyingContract: armor.address,
      version: '1',
    },

    message: {
      owner: owner.address,
      tokenId: "5",
      value: value,
      nonce: nonce
    },
    primaryType: 'PermitMintEther',
    types: {
      EIP712Domain: [
        { name: 'name', type: 'string' },
        { name: 'version', type: 'string' },
        { name: 'chainId', type: 'uint256' },
        { name: 'verifyingContract', type: 'address' },
      ],
      PermitMintEther: [
        { name: 'owner', type: 'address' },
        { name: 'tokenId', type: 'uint256' },
        { name: 'value', type: 'uint256' },
        { name: 'nonce', type: 'uint256' }
      ]
    },
  }
    types=typedData.types


    const accounts = hre.config.networks.hardhat.accounts;
    const index = 0; // first wallet, increment for next wallets
    const wallet1 = ethers.Wallet.fromMnemonic(accounts.mnemonic, accounts.path + `/${index}`);
    const hash=signHash()
    const signature = ethUtil.ecsign(hash, ethUtil.toBuffer(wallet1.privateKey))

    const r = signature.r
    const s = signature.s
    const v = signature.v

    const tx=await armor.permitMintForEthers(owner.address, 5, value, v, r, s,{ value: value })
    const receipt = await tx.wait()
    for (const event of receipt.events) {
        console.log(`Event ${event.event} with args ${event.args}`);
    }

      expect( await armor.ownerOf("5")).to.equal(owner.address) ;
    });

});
