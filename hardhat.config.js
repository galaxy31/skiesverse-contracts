require('dotenv').config();
require("@nomicfoundation/hardhat-toolbox");
require("@openzeppelin/hardhat-upgrades")
require("@nomicfoundation/hardhat-chai-matchers");
require("hardhat-gas-reporter");


/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.9",
  settings: {
    optimizer: {
      enabled: true,
      runs: 200
    }
  },
  networks: {
    development: {
      url: `http://brpg-meta.galaxy4g.com:8544`
    },
    mumbai: {
      url: "https://rpc-mumbai.maticvigil.com",
      accounts: ["bc19e817366c3224c8d2a530013f23da598069b0c476b81c29f8e219ddbc7446"]
    },
    mumbai2: {
      url: "https://matic-mumbai.chainstacklabs.com",
      accounts: ["bc19e817366c3224c8d2a530013f23da598069b0c476b81c29f8e219ddbc7446"]
    }
  },
  etherscan: {
    apiKey: "FYEYD2NRJC4J61J4RKA4K6FQTZ48JGX7US"
  },
  gasReporter: {
    currency: 'USD',
    token: 'MATIC',
//    gasPriceApi: 'https://api.bscscan.com/api?module=proxy&action=eth_gasPrice',
    coinmarketcap: (process.env.COINMARKETAPIKEY),
    gasPrice: 120,
    enabled: (process.env.REPORT_GAS) ? true : false
  }


};
//bc19e817366c3224c8d2a530013f23da598069b0c476b81c29f8e219ddbc7446
//07abea61900863aa4ccc889db18aba00c2fd730bdd87f503e4e8e25d8f45690d